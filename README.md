# README #

Another "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon. It is a basic flask server that can serve up static pages and some errors.

## Instructions:
---------------

* Download the repo and build the docker image using the Dockerfile found in the 'web' folder.

* Use docker to run the container.

	* For some basic docker help check out the [unmodified version of this repo.](https://bitbucket.org/UOCIS322/proj2-pageserver/)

* Go to 0.0.0.0:5000/trivia.html and bask in the glory, again.

	* Place your own files in the 'static' folder to see them in a similar fashion.

	* Also check out the [Flask documentation](http://flask.pocoo.org/docs/1.0) if you're looking to change things up further.

## Authors
---------------

### Original Version:
* **UOCIS322** [proj2-pageserver](https://bitbucket.org/UOCIS322/proj2-pageserver/)

### Updates to serve files and errors:

* **Cole Sabin** [E-Mail](mailto:csabin@uoregon.edu)

